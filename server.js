var express = require('express')

var app = express()

app.use(express.static('public'))

app.get('/', function (request, response){
    response.send('<h3>Web Socket<h3>')
})


var server = app.listen(3000, function(){
    console.log('Listening on request on port 3000')
})

var socket = require('socket.io')
var io = socket(server);
io.on('connection', function(socket){
    socket.on('chat', function(data){
        io.emit('chat', data)  
    })
})